#include <algorithm>
#include <cstdint>
#include <fstream>
#include <functional>
#include <future>
#include <iostream>
#include <iterator>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

bool is_prime(uint64_t p) {
    if (p == 1) {
        return false;
    }
    for (uint64_t i = 2; i*i <= p; ++i) {
        if(!(p % i)) {
            return false;
        }
    }
    return true;
}

void thread_func(unsigned int count, uint64_t low, uint64_t high, std::vector<std::promise<uint64_t>> &prom) {
    unsigned int amount = 0;
    for (uint64_t i = low; i <= high && count > 0; ++i) {
        try {
            if (i == high) {
                throw high;
            }
        } catch (...) {
            prom[amount].set_exception(std::current_exception());
            return;
        }
        if (is_prime(i)) {
            prom[amount++].set_value(i);
        }
        if (amount == count) {
            return;
        }
    }
}

int main(int argc, char *argv[]) {
    uint64_t low, high;
    unsigned int count;
    std::cin >> low >> high >> count;
    std::vector<std::promise<uint64_t>> prom(count);
    std::vector<std::future<uint64_t>> fut(count);
    std::thread thr(thread_func, count, low, high, std::ref(prom));
    uint64_t prime;
    for (unsigned int i = 0; i < count; ++i) {
        fut[i] = prom[i].get_future();
        try {
            prime = fut[i].get();
            std::cout << prime << std::endl;
        } catch (uint64_t exc) {
            std::cout << exc << std::endl;
            break;
        }
    }
    thr.join();
    return 0;
}

