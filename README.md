#### Code sample

Task: output of **count** prime numbers in the range from **low** to **high** using threads

Compilation: g++ -std=gnu++11 -Wall -pthread task.cpp -o task

Execution: ./task

Input: low, high, count
